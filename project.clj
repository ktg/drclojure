(defproject DrClojure "0.1.18"
  :description "DrClojure"
  :url "http://steloflute.tistory.com/entry/DrClojure"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :main DrClojure.core
  :aot :all)
